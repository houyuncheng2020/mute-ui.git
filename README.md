## Introduction
mute-ui是一个以学习为目的的React UI组件库，主要目的是学习组件库开发、测试、静态文档编写生成、npm包发布的一般流程。

使用 npm + create-react-app 的工作流。

支持 TypeScript，CSS与编译器使用sass。

文档生成工具使用docz。


## 📦 Install

```bash
npm install mute-ui
```

```bash
yarn add mute-ui
```

## 🔨 Usage

```jsx
import { Button } from 'mute-ui';

const App = () => (
  <>
    <Button type="primary">PRESS ME</Button>
  </>
);
```

And import style manually:

```jsx
import 'mute-ui/dist/index.css';
```

## Documentation
[mute-ui](https://houyuncheng2020.gitee.io/mute-ui/)

## License

MIT