export default {
  files: ['./src/components/**/*.mdx','./src/*.mdx'], // 匹配要处理的文件
  dest: 'doc-site', // 打包出来的文件目录名
  title: 'mute-ui', // 站点标题
  typescript: true, // ts 项目需开启
  menu: ['快速上手', '业务组件'],
  base: '/mute-ui'
}