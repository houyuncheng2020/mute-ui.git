import React from 'react';

type SpaceProps = {
  children?: React.ReactNode |React.ReactNode[],
}

const Space = (props: SpaceProps): JSX.Element => {

  let {
    children = []
  } = props;

  let items: any = null;
  if (Object.prototype.toString.call(children) === '[object Object]') {
    items = [children];
  } else {
    items = children;
  }

  const wrappers = items.map((item: React.ReactNode, idx: number) => {
    return (
      <div key={idx} className="mute-space-item">
        {item}
      </div>
    );
  })

  return (
    <div className="mute-space">
      {wrappers}
    </div>
  )
}

export default Space;