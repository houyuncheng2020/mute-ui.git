import { forwardRef, useImperativeHandle, useRef } from 'react';
import Input, { InputProps } from './input';


interface AddonProps extends InputProps {
  addonBefore?: string,
  addonAfter?: string,
}

const Addon = forwardRef((props: AddonProps, cref): JSX.Element => {

  const {
    addonBefore = '',
    addonAfter = '',
    value,
    ...rest
  } = props;


  useImperativeHandle(cref, () => ({
    ...ref.current,
    value: addonBefore + ref.current.value + addonAfter
  }));

  const ref = useRef<HTMLInputElement>(null!);

  return (
    addonAfter || addonBefore ? (
      <div className="mute-input-wrapper">
        {
          addonBefore && <div className="mute-input-addonbefore">{addonBefore}</div>
        }
        <Input className="mute-input" ref={ref} {...rest} value={value} />
        {
          addonAfter && <div className="mute-input-addonafter">{addonAfter}</div>
        }
      </div>
    )
      :
      <Input ref={ref} {...rest} value={value} />
  )
})

export default Addon;